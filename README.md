# Introduction to Algebra
R. Rusczyk, _Introduction to Algebra_, 2nd ed. San Diego, CA, USA: AoPS, 2017.

## 1. Follow the Rules

The __integers__ are the whole numbers we use for counting, and their opposites: $`\dots,-4,-3,-2,1,0,1,2,3,4,\dots`$

Numbers greater than 0 are __positive__ and numbers less than 0 are __negative__.

__WARNING!!__
The number 0 is neither negative nor positive. It is __nonnegative__ (that is, not negative), as are all positive integers, and it is also __nonpositive__, as are the negative integers.

When we divide one integer by another nonzero integer, we form a __rational number__. All integers are rational numbers, but not all rational numbers are integers! For example, 1/2 is not an integer, but is a rational number.

Not all numbers are rational. Numbers that cannot be written as one integer divided by another are called __irrational numbers__. For example, the square root of 2 is irrational.

The square root of a negative number is called an __imaginary number__.

The __real numbers__ are the numbers we can write as a decimal, even if we have to use infinitely many decimal places. All integers, rational numbers and irrational numbers are real numbers.

---

By using __arithmetic__, we combine numbers to form other numbers.

__WARNING!!__ You cannot divide by 0.

The rules for evaluating a mathematical expression with multiple operations are called the __order of operations__. Here they are:
1. If there are parentheses in the expression, evaluate all expressions within parentheses, working from the inside out. Compute each expression inside parentheses using the order of operations.
2. Perform all exponentiations.
3. Perform all multiplications and divisions from left to right.
4. Perform all additions and subtractions from left to right.

---

Addition is __commutative__, which means we can reverse the order of two numbers being added without changing their sum. In other words, for any numbers $`a`$ and $`b`$, we have

```math
a+b=b+a
```

Multiplication is __commutative__, which means we can reverse the order of two numbers being multiplied without changing their product. In other words, for any numbers $`a`$ and $`b`$, we have

```math
a\cdot b=b\cdot a
```

__WARNING!!__ Subtraction and division are not commutative.

Subtraction is __not commutative__, because reversing the order usually changes the result of subtraction: $`2-5=\not5-2`$. If we think of subtraction as addition of a negative number, we can then use the __commutative property of addition__ to reverse the numbers being added.

```math
a-b=a+(-b)
```

Just as we can turn subtraction into addition of a negative number, we can turn division into multiplication, then use the __commutative property of multiplication__.

```math
a/b=a\cdot\frac{1}{b}
```

The __associative property of addition__ tells us that for any three numbers $`a,b`$, and $`c`$, we have

```math
(a+b)+c=a+(b+c)
```

The __associative property of multiplication__ tells us that for any numbers $`a,b`$, and $`c`$, we have

```math
(a\cdot b)\cdot c=a \cdot (b\cdot c)
```

__WARNING!!__ When reordering a group of numbers that involves addition and subtraction, be careful to keep track of the negative signs. Think of subtraction as addition of a negative number, and you'll be careful not to make a mistake like rewriting

```math
(2-6)+(-5+7)
```

as

```math
2+7+6-5
```

Notice that the sign of 6 has been mistakenly changed from negative to positive!

Don't make computations harder than they need to be! Rearrange numbers in sums and products using the commutative and associative properties in ways that make the calculations easier.

---

For any three numbers $`a,b`$, and $`c`$, the __distributive property__ states that

```math
a\cdot(b+c)=a\cdot b+a\cdot c
```

Reversing the distributive property is called __factoring__, we take a common factor out of each term in a sum and write the result as this factor times a simpler sum:

```math
a\cdot b+a\cdot c=a\cdot(b+c)
```

If the numerator and denominator of a fraction have a common factor, then that factor can be cancelled from both the numerator and the denominator.

```math
\frac{4}{4\cdot9}=\frac{\cancel{4}\cdot1}{\cancel{4}\cdot9}=\frac{1}{9}
```

__WARNING!!__ Canceling only works when the numerator and denominator are products. (If either is just a single number alone, we can think of it as the product of that number and 1.) We cannot cancel a term of a sum in either the numerator or denominator. For example, this is __not__ valid:

```math
\frac{3+9}{2+9}=\frac{3+\cancel{9}}{2+\cancel{9}}
```

You can use the distributive property to perform some products quickly in your head! For example,

```math
\begin{aligned}
8\cdot89&=8\cdot(90-1)=8\cdot90-8\cdot1=720-8=712\\
21\cdot398&=21\cdot(400-2)=21\cdot400-21\cdot2=8400-42=8358
\end{aligned}
```

---

We can write an __equation__ when two mathematical expressions are equal.

the __symmetric property of equality__ tells us that we can reverse the sides of an equation to get another valid equation, if $`a=b`$, then $`b=a`$.

If $`c`$ is any number and $`a=b`$ then all of the following equations are true:

```math
a+c=b+c\qquad a-c=b-c\qquad a\cdot c=b\cdot c\qquad \frac{a}{c}=\frac{b}{c}
```

(We must have $`c`$ not equal to 0 for the last equation to be valid.)

If $`\frac a b=\frac c d`$, then $`ad=bc`$

```math
\begin{aligned}
\frac a b&=\frac c d\\
b\cdot \frac a b&=b \cdot \frac c d\\
\frac {\cancel{b}\cdot a} {\cancel{b}}&=\frac {b \cdot c} d\\
a&=\frac{b\cdot c}{d}\\
a\cdot d&=\frac {b \cdot c} d \cdot d\\
ad&=\frac {b \cdot c\cdot \cancel{d}} {\cancel{d}}\\
ad&=bc
\end{aligned}
```

This equation manipulation is often referred to as __cross-multiplying__.

If $`a=b`$ and $`c=d`$, then

```math
a+c=b+d \qquad a-c=b-d \qquad\text{and}\qquad ac=bd
```

The __transitive property of equality__ tells us that if one expression is equal to two other expressions, then these two other expressions are equal. If $`a=b`$ and $`b=c`$, then

```math
a=c
```

If we take the reciprocal of both sides of an equation that both do not equal zero, then the two reciprocals are equal. If $`a=b`$, then $`\frac{1}{a}=\frac{1}{b}`$.

```math
\begin{aligned}
a&=b\\
\frac{\cancel{a}}{\cancel{a}}&=\frac b a\\
1&=\frac{b}{a}\\
\frac{1}{b}&=\frac{\frac{b}{a}}{b}\\
\frac{1}{b}&=\frac{\cancel{b}}{a\cancel{b}}\\
\frac{1}{b}&=\frac{1}{a}
\end{aligned}
```

The quotient of the left sides of two equations equals the quotient of the right sides of the equations (as long as we aren't dividing by 0!). If $`a=b`$ and $`c=d`$, and $`a=\not0`$, $`c=\not0`$ then $`\frac{a}{c}=\frac{b}{d}`$.

```math
\begin{aligned}
c&=d\\
\frac{1}{c}&=\frac{1}{d}\\
a\cdot\frac{1}{c}&=b\cdot\frac{1}{d}\\
\frac{a}{c}&=\frac{b}{d}
\end{aligned}
```

---

In $`3^6`$ the 6 is an __exponent__ and the 3 is the a __base__. It can be read as "3 to the 6<sup>th</sup> __power__" or "3 raised to the 6<sup>th</sup> power" or simply "3 to the 6<sup>th</sup>".

If we multiply $`b`$ copies of $`a`$ by $`c`$ copies of $`a`$, we get a product of $`b+c`$ copies of a.

```math
a^b\cdot a^c=a^{b+c}
```

__WARNING!!__ The bases in the expressions of the exponent law $`a^b\cdot a^c=a^{b+c}`$ are all the same. We cannot simplify an expression like $`2^3\cdot 5^6`$ by simply adding the exponents.

the expression $`(a^b)^c`$ tells us to multiply $`c`$ copies of $`a^b`$. Since $`a^b`$ is the product of $`b`$ copies of $`a`$, the product of $`c`$ copies of $`a^b`$ is the product of $`bc`$ copies of $`a`$, and we have

```math
(a^b)^c=a^{bc}
```

If $`b`$ and $`c`$ are positive integers with $`c<b`$, then in the expression $`\frac{a^b}{a^c}`$, the $`c`$ factors of $`a`$ in the denominator cancel with $`c`$ of the $`b`$ factors of $`a`$ in the numerator, leaving a product of $`b-c`$ copies of $`a`$.

```math
\frac{a^b}{a^c}=a^{b-c}
```

Similarly, if $`a`$ is nonzero, we have

```math
a^{-b}=\frac{1}{a^b}
```

Any nonzero number raised to the 0<sup>th</sup> power is 1.

```math
a^{0}=1
```

__WARNING!!__ Some sources define $`0^0`$ as 1, and others leave it undefined.

Whenever we raise a product to a power, the result is the product of raising each term in the original product to that power.

```math
(a\cdot b)^p=a^p\cdot b^p
```

A number raised to a negative power equals the reciprocal of that number raised to the _opposite_ of the power.

```math
a^{-b}=\frac{1}{a^b}=\left(\frac{1}{a}\right)^b
```

If $`a`$ is nonzero, then

```math
a^b \cdot a^c=a^{b+c}\newline
(a^b)^c=a^{bc}\newline
\frac{a^b}{a^c}=a^{b-c}\newline
a^{-p}=\frac{1}{a^p}\newline
a^0=1\newline
(ab)^p=a^p \cdot b^p
```

Multiplying an even number of negative numbers gives a positive number, but multiplying an odd number of negative numbers gives a negative number.

```math
(-2)^4=(-2)\cdot(-2)\cdot(-2)\cdot(-2)=16\newline
(-3)^5=(-3)\cdot(-3)\cdot(-3)\cdot(-3)\cdot(-3)=-243
```

---

If $`x`$ is nonnegative, then $`x^{\frac 1 2}`$ is the nonnegative number whose square is $`x`$.

__WARNING!!__ While it is true that $`(-5)^{2}=25`$, the expression $`25^{\frac 1 2}`$ is _not_ equal to -5. We define $`25^{\frac 1 2}`$ to be the _nonnegative_ number whose square is 25.

We can separate the numerator and denominator in an exponent.

```math
a^{\frac m n} = (a^m)^{\frac 1 n}\quad \text{or} \quad (a^{\frac 1 n})^{m}
```

If $`n`$ is _even_ and $`x`$ is negative, then we cannot evaluate $`x^{\frac 1 n}`$, since an even power of any positive or negative number (or 0) is nonnegative.

---

The symbol $`\sqrt{\space}`$ is called a __radical__ and stands for raising a number to the $`\frac{1}{2}`$ power. In general, if $`x`$ is nonnegative, then $`\sqrt{x}`$ equals the number whose square is $`x`$. For any nonnegative number $`a`$, we have

```math
\sqrt{a^2}=a
```

__WARNING!!__ The equation $`\sqrt{a^2}=a`$ is only true if $`a`$ is nonnegative. It is not true if $`a`$ is negative! The square root of a nonnegative number is defined to be nonnegative.

We can use radicals to represent powers besides $`\frac{1}{2}`$. We can write $`2^{\frac 1 n}`$ as $`\sqrt[n]{2}`$ for any positive integer $`n`$ that is greater than 2.

If $`n`$ is a positive integer with $`n>2`$, then

```math
x^{\frac m n} = \sqrt[n]{x^m} = (\sqrt[n]{x})^m
```

Don't make computations harder than they need to be! Rearrange numbers in sums and products using the commutative and associative properties in ways that make the calculations easier.

---

These __divisibility rules__ help determine when positive integers are divisible by particular other integers. All of these rules apply for base-10 _only_ - other bases have their own, different versions of these rules.

|||
|----|----|
|2, $`2^n`$|A number is divisible by $`2^n`$ if and only if the last $`n`$ digits of the number are divisible by $`2^n`$. Thus, in particular, a number is divisible by 2 if and only if its units digit is divisible by 2, i.e. if the number ends in 0, 2, 4, 6 or 8.|
|3, 9|A number is divisible by 3 or 9 if and only if the sum of its digits is divisible by 3 or 9, respectively. Note that this does not work for higher powers of 3. For instance, the sum of the digits of 1899 is divisible by 27, but 1899 is not itself divisible by 27.|
|5, $`5^n`$|A number is divisible by $`5^n`$ if and only if the last $`n`$ digits are divisible by that power of 5.|
|7|Rule 1: Partition $`N`$ into 3 digit numbers from the right ($`d_3d_2d_1,d_6d_5d_4,\dots`$). The alternating sum ($`d_3d_2d_1 - d_6d_5d_4 + d_9d_8d_7 - \dots`$) is divisible by 7 if and only if $`N`$ is divisible by 7.
||Rule 2: Truncate the last digit of $`N`$, double that digit, and subtract it from the rest of the number (or vice-versa). $`N`$ is divisible by 7 if and only if the result is divisible by 7.|
||Rule 3: "Tail-End divisibility." Note. This only tells you if it is divisible and NOT the remainder. Take a number say 12345. Look at the last digit and add or subtract a multiple of 7 to make it zero. In this case we get 12380 or 12310 (both are acceptable; I am using the former). Lop off the ending 0's and repeat. 1238 - 28 ==> 1210 ==> 121 - 21 ==> 100 ==> 1 NOPE. Works in general with numbers that are relatively prime to the base (and works GREAT in binary). Here's one that works. 12348 - 28 ==> 12320 ==> 1232 +28 ==> 1260 ==> 126 + 14 ==> 14 YAY!|
|10, $`10^n`$|If a number is power of 10, define it as a power of 10. The exponent is the number of zeros that should be at the end of a number for it to be divisible by that power of 10. Example: A number needs to have 6 zeroes at the end of it to be divisible by 1,000,000 because $`1,000,000=10^6`$. |

https://artofproblemsolving.com/wiki/index.php/Divisibility_rules

## 2. x Marks the spot

A __variable__ is a placeholder, which means it can take on any value.  
A __constant__ always has the same value; it can only ever have one specific value.

When we combine numbers and/or variables using operations we form a mathematical __expression__.

We call the product of a constant and a variable raised to some power a __term__. The constant is called the __coefficient__ of the term.

__WARNING!!__ When a constant is multiplied by a variable that is raised to a power, only the variable is raised to the power!

__WARNING!!__ We have to keep careful track of our signs when rearranging a group of numbers that we are adding and subtracting. For example, we cannot rearrange

```math
(3r-2)+(3r-4)+(7-5r)
```

into

```math
3r+3r+5r+2-4+7
```

In this mistaken rearrangement, we've accidentally changed the sign of -2 into +2 and changed the -5r to +5r.

__WARNING!!__ Although $`(2x)^3 = 2^3x^3`$ and $`(2+x)\cdot3=2\cdot3+x\cdot3`$, it is _not true_ that $`(2+x)^3`$ equals $`2^3+x^3`$.

__WARNING!!__ When simplifying a fraction in which the numerator and denominator are products, such as $`\frac{3c}{5c}`$, we can "cancel" the common factors, like this:

```math
\frac{3c}{5c}=\frac{3\cancel{c}}{5\cancel{c}}=\frac{3}{5}
```

This works because we can write $`\frac{3c}{5c}`$ as $`\frac{3}{5}\cdot \frac{c}{c}`$, and $`\frac{c}{c}`$ equals 1. However, we cannot "cancel" terms in the numerator and denominator when the numerator or denominator is a sum. For example, we __cannot__ cancel $`c`$ from the numerator and denominator of $`\frac{3+2c}{5+3c}`$:

```math
\frac{3+2c}{5+3c} \not = \frac{3+2}{5+3}
```

While we can write $`\frac{3c}{5c}`$ as $`\frac{3}{5}\cdot \frac{c}{c}`$, we can't rewrite $`\frac{3+2c}{5+3c}`$ in a similarly convenient way. For example, $`\frac{3+2c}{5+3c}`$ does _not_ necessarily equal $`\frac{3+2}{5+3} \cdot \frac{c}{c}`$. So, we can't cancel the $`c`$'s in $`\frac{3+2c}{5+3c}`$.

__WARNING!!__ If we start with $`\sqrt[3]{27+x^9}`$, we cannot separate 27 and $`x^9`$ into two different radicals in the same way we can write $`\sqrt[3]{27x^9}`$ as $`\sqrt[3]{27}\cdot \sqrt[3]{x^9}`$. Writing $`\sqrt[3]{27x^9}`$ as $`\sqrt[3]{27}\cdot \sqrt[3]{x^9}`$ is just an application of a law of exponents:

```math
(27x^9)^{1/3}=(27^{1/3})(x^9)^{1/3}
```

We don't have a similar law of exponents for raising a sum to a power. As an example of why we can't simply separate a sum raised to a power the same way we separate a product, notice that $`(16+9)^{1/2}`$ equals 5, but $`16^{1/2}+9^{1/2}`$ equals 7.

__WARNING!!__ The equation $`\sqrt{a^2}=a`$ is true only if $`a`$ is nonnegative. If we do not know the sign of $`a`$, then we cannot simplify $`\sqrt{a^2}`$ to $`a`$.

---

__WARNING!!__ If two terms do not have the same variable expressions, we cannot add them by simply adding their coefficients. So, while we can add $`5x^2`$ and $`6x^2`$ by adding 5 and 6,

```math
5x^2+6x^2=(5+6)x^2=11x^2
```

we cannot add $`5x^2`$ and $`6x`$ by adding 5 and 6. If we tried to do so, what would the 5+6=11 be multiplied by?!?

Substitutions often simplify algebraic expression and clarify solutions.

---

__Important:__ Factoring can help simplify fractions when the numerator and denominator have variables.

We can apply the properties of arithmetic to expressions involving variables. For example, we can use the distributive property to add terms that have the same variable expression:

```math
3x^2+5x^2=(3+5)x^2=8x^2
```

We add fractions involving variables just as we add constant fractions - by finding a common denominator.

```math
\frac{x}{3}+\frac{2}{x}=\frac{x}{3}\cdot\frac{x}{x}+\frac{2}{x}\cdot\frac{3}{3}=\frac{x\cdot x}{3x}+\frac{2\cdot3}{3x}=\frac{x^2+6}{3x}
```

---
The product of two powers of an expression equals the expression raised to the sum of the two exponents. For example,
```math
a^4\cdot a^8=a^{12} \qquad 3^5\cdot3^3=3^8 \qquad x\cdot x^9=x^{10}
```

If an expression raised to a power is itself raised to another power, then the result is the expression raised to the product of the powers. For example,
```math
(x^2)^4=x^{2\cdot4}=x^8 \qquad (3^5)^7=3^{5\cdot7}=3^{35} \qquad (2^3)^4=2^{3\cdot4}=x^{12}
```

When a product of a group of numbers is raised to a power, the result is the product of each number in the group raised to that power. For example,
```math
(3z)^4=3^{4}\cdot z^4 \qquad (-5a)^7=(-5)^{7}\cdot a^{7} \qquad (3x^9)^3=3^3\cdot (x^9)^3
```

A fraction (or quotient) consisting of two powers of the same expression equals that expression raised to the power equal to the power of the numerator minus the power of the denominator. For example.
```math
\frac{t^8}{t^3}=t^{8-3}=t^5 \qquad \frac{4^6}{4^3}=4^{6-3}=4^3 \qquad \frac{p^5}{p^3}=p^{5-3}=p^2
```

If _a_ ≠ 0, then
```math
a^{-p}=\frac{1}{a^p}
```



## 3. One-Variable Linear Equations
The "one-variable" means that only one variable appears in the equation, though it may appear multiple times.
The "linear" means that the variable only appears as a constant times the first power of the variable.

Isolate, isolate, isolate. The key to solving many equations is to get the variable alone on one side of the equation.

When solving an equation, we can check our answer by substituting it back into the original equation. If the original equation is not satisfied by our answer, then we made a mistake.

When the variable in a linear equation has a coefficient besides 1, we can multiply both sides of the equation by the reciprocal of the variable's coefficient to help isolate the variable.

If you don't like dealing with fractions, you can eliminate fractions from a linear equation by multiplying both sides of the equation by the least common denominator of the fractions in the equation.

__Important:__ If a linear equation can be manipulated into an equation that is never true (such as -1=-5), then there are no solutions to the equation. Similarly, if a linear equation can be manipulated into an equation that is always true (such as 4r+5=4r+5), then all possible values of the variable are solutions to the original equation.

Much of understanding and applying algebra requires recognizing general forms. It's easy to see that 3y-2=30-y is a linear equation. The next step is to understand that an equations like
```math
3\sqrt{x}-2=30-\sqrt{x}
```
can also be treated like a linear equation. The expression we solve for first in this equation is $`\sqrt{x}`$, rather than just a variable. After we find $`\sqrt{x}`$, we can easily find x. Substitution is a powerful tool that helps us recognize forms. By substituting a simple variable _y_ for the more complex expression $`\sqrt{x}`$, we can see how to solve $`3\sqrt{x}-2=30-\sqrt{x}`$.

Equations in which a variable appears inside a radical are often solved by raising the equation to the appropriate power. For example, equations with a variable inside a root are often tackled by squaring the equating, and equations with a variable inside of a cube root usually require cubing the equation.

__WARNING!!__ If you raise an equation to an even power, you must check your solution at the end to make sure it isn't an __extraneous solution__; a solution that does not satisfy the original equation.

__WARNING!!__ Equations in which a variable appears in a denominator can have extraneous solutions, just as equations with variables inside a square root can.

## 4. More Variables
When we combine added or subtracted terms that have the same variable expressions, we say we are __combining like terms__.

Just as with one-variable expressions, we can factor out entire expressions from a sum or difference. For example, we can factor ab(a+1) - 3(a+1) by noticing that (a+1) is a factor of both terms:

```math
ab(a+1)-3(a+1)=(ab-3)(a+1)
```

Here's a geometric look at factoring; see if you can figure out how it works:
```math
\def\arraystretch{1.5}
    \begin{array}{cc}
        a&
        \begin{array}{|c|}
            \hline
            am \\ \hline
        \end{array}\\
        &m
    \end{array}
+
\def\arraystretch{1.5}
    \begin{array}{cc}
        a&
            \begin{array}{|c|}
            \hline
            an \\ \hline
        \end{array}\\
        &n
    \end{array}
=
\def\arraystretch{1.5}
    \begin{array}{cccc}
    a
    &\begin{array}{|c:}
        \hline
        \\ \hline
    \end{array}
    &\begin{array}{:c|}
        \hline
        a(m+n) \\ \hline
    \end{array}\\
    &m&n\\
\end{array}
```
## 5. Multi-Variable Linear Equations
A group of equations for which we seek values that satisfy all of the equations at the same time is called a __system of equations__.
